Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'memos#index'
  get 'top/login'
  post 'top/login'
  get 'top/logout'
  get 'memos/index'
  get 'memos/new'
  resources :memos
  resources :likes
  resources :users
end
