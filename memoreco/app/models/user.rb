class User < ApplicationRecord
    def self.authenticate(uid, pass)
        user = User.find_by(uid: uid)
        if user and BCrypt::Password.new(user.pass) == pass
            user
        else
            nil
        end
    end
  
    def password=(val)
        if val.present?
            self.pass = BCrypt::Password.create(val)
        end
        @password = val
    end
end
