class ApplicationController < ActionController::Base
    private
        def current_user
            if session[:login_uid]
                User.find_by(uid: session[:login_uid])
            end
        end
        def create
            @post = Post.new(
            content: params[:content],
            user_id: @current_user.id)
        end
        helper_method :current_user
end
