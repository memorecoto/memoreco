class MemosController < ApplicationController
    def index
        if current_user
            @memos = Memo.all
        else
            redirect_to top_login_path
        end
    end
    def new
        @memo = Memo.new
    end
    def show
        @memo = Memo.find(params[:id])
    end
    def edit
        @memo = Memo.find(params[:id])
    end
    def update
        @memo = Memo.find(params[:id])
        if @memo.update(
            title: params[:memo][:title],
            directer: params[:memo][:directer],
            release: params[:memo][:release],
            memo: params[:memo][:memo])
            redirect_to root_path
        else
            render :edit
        end
    end
    def create
        @memo = Memo.new(
            user_id: User.find_by(uid: session[:login_uid]).id,
            title: params[:memo][:title],
            directer: params[:memo][:directer],
            release: params[:memo][:release],
            memo: params[:memo][:memo])
        if @memo.save
            redirect_to root_path
        else
            render new_memo_path
        end
    end
    def destroy
        memo = Memo.find(params[:id])
        memo.destroy
        redirect_to '/'
    end
end
