class LikesController < ApplicationController
  def create
    memo = Memo.find(params[:memo_id])
    unless memo.liked?(current_user)
      memo.like(current_user)
    end
    redirect_to root_path
  end

  def destroy
    memo = Memo.find(params[:id])
    if memo.liked?(current_user)
      memo.unlike(current_user)
    end
    redirect_to root_path
  end
end
